import java.util.ArrayList;
import java.util.Random;

public class Game {
	double population;
	double stability; // grade 1-5
	double severity; // grade 1-5
	boolean conflict; // true = conflict, 
	ArrayList<Behaviour> bList = new ArrayList<>();
	double meanC;
	double donation;
	
	Random rand=new Random();

	public Game(double donation, double pop, double stability, double severity, boolean conflict) {
		this.donation=donation;
		this.population=pop;
		this.stability=stability;
		this.severity=severity;
		this.conflict=conflict;
	}
	
	
	public ArrayList<Behaviour> makeVillage(){
		double donationpp=donation/population;
		Behaviour be= new Behaviour("SAVER",donationpp);
		bList.add(be);
		be= new Behaviour("SPENDER",donationpp);
		bList.add(be);
		be= new Behaviour("IMPORTER",donationpp);
		bList.add(be);
		be= new Behaviour("ENTREPRENOUR",donationpp);
		bList.add(be);
		be= new Behaviour("TRADER",donationpp);
		bList.add(be);
		be= new Behaviour("CHILD",donationpp);
		bList.add(be);
		
		normalizeList();
		
		return bList;
	}
	
	private void normalizeList(){ //Update proc in Behaviors
		double tot=0;
		for (Behaviour be : bList){
			tot=tot+be.getProc();
		}
	
		for (Behaviour be : bList){
			be.setProc(be.getProc()*(100/tot));
		}
	}
	
	public double calcC(){
		double p1=bList.get(0).getProc();
		double p2=bList.get(1).getProc();
		double p3=bList.get(2).getProc();
		double totProc=p1+p2+p3;
		
		double c1=bList.get(0).getC();
		double c2=bList.get(1).getC();
		double c3=bList.get(2).getC();
		
		double meanC=(p1*c1+p2*c2+p3*c3)/totProc;
		
		// Population (max diff: +-20%)
		if (population<5000){
			meanC=meanC*0.80;
		}
		if (population>50000){
			meanC=meanC*1.20;
		}
		
		// STability 1-5 (max diff: +-15%)
		meanC=((stability-2)/20+1)*meanC;
		
		// Severity 1-5 (max diff: +-15%)
		meanC=((3-severity)/20+1)*meanC;
		
		// Market impact
		double p4=bList.get(3).getProc();
		double p5=bList.get(4).getProc();
		double procMark=p4+p5;
		meanC=((procMark-38)/80+1)*meanC;
		
		// Children impact
		double p6=bList.get(5).getProc();
		meanC=((20-p6)/80+1)*meanC;
		
		// hazard impact
		if(conflict==false){
			meanC=meanC*0.85;
		}
		
		meanC=Math.max(0.1,meanC);
		meanC=Math.min(0.9,meanC);
		
		return meanC;
	}
	
	public double calcME(double c) {
        return 1/(1-c);
    }
	
	public String toString(){
		StringBuilder sb= new StringBuilder();
		
		for (Behaviour be:bList){
			sb.append(be.getName()+" "+be.getProc()+"% "+be.getC()+"\n");
		}
		
		return sb.toString();
	}
	
	public ArrayList<Behaviour> getList(){
		return bList;
	}


    public double getProc(int i) {
        return bList.get(i).getProc();
    }


}
