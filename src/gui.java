/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;

import org.jfree.chart.ChartFactory;
import java.awt.Font;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Miriam
 */
public class gui extends javax.swing.JFrame {

    /**
     * Creates new form MainWindow
     */
    public gui() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        inputPanel = new javax.swing.JPanel();
        donationField = new javax.swing.JTextField();
        sizeField = new javax.swing.JTextField();
        calamity = new javax.swing.JComboBox<>();
        donationLabel = new javax.swing.JLabel();
        populationLabel = new javax.swing.JLabel();
        crisisLabel = new javax.swing.JLabel();
        stabilitySlider = new javax.swing.JSlider();
        stabilityLabel = new javax.swing.JLabel();
        severitySlider = new javax.swing.JSlider();
        severityLabel = new javax.swing.JLabel();
        PlayButton = new javax.swing.JButton();
        displayLabel = new javax.swing.JLabel();
        resultPanel = new javax.swing.JPanel();
        showLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        inputPanel.setBackground(new java.awt.Color(253, 223, 0));
        inputPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        inputPanel.setForeground(new java.awt.Color(204, 51, 0));
        inputPanel.setName("inputPanel"); // NOI18N

        donationField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                donationFieldKeyTyped(evt);
            }
        });

        sizeField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                sizeFieldKeyTyped(evt);
            }
        });

        calamity.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Constant stress", "Acute shock" }));

        donationLabel.setText("Enter donation:");

        populationLabel.setText("Enter population:");

        crisisLabel.setText("Choose type of crisis:");

        stabilitySlider.setBackground(new java.awt.Color(253, 223, 0));
        stabilitySlider.setMajorTickSpacing(1);
        stabilitySlider.setMaximum(5);
        stabilitySlider.setMinimum(1);
        stabilitySlider.setMinorTickSpacing(1);
        stabilitySlider.setPaintLabels(true);
        stabilitySlider.setPaintTicks(true);
        stabilitySlider.setSnapToTicks(true);
        stabilitySlider.setToolTipText("");
        stabilitySlider.setValue(3);
        stabilitySlider.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        stabilityLabel.setText("Stability of community:");

        severitySlider.setBackground(new java.awt.Color(253, 223, 0));
        severitySlider.setMajorTickSpacing(1);
        severitySlider.setMaximum(5);
        severitySlider.setMinimum(1);
        severitySlider.setMinorTickSpacing(1);
        severitySlider.setPaintLabels(true);
        severitySlider.setPaintTicks(true);
        severitySlider.setSnapToTicks(true);
        severitySlider.setToolTipText("");
        severitySlider.setValue(3);
        severitySlider.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        severityLabel.setText("Severity of crisis:");

        PlayButton.setText("Play");
        PlayButton.setOpaque(false);
        PlayButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PlayButtonActionPerformed(evt);
            }
        });

        displayLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        displayLabel.setText("0.0");

        resultPanel.setBackground(new java.awt.Color(253, 223, 0));

        showLabel.setText(" ");

        javax.swing.GroupLayout resultPanelLayout = new javax.swing.GroupLayout(resultPanel);
        resultPanel.setLayout(resultPanelLayout);
        resultPanelLayout.setHorizontalGroup(
            resultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, resultPanelLayout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addComponent(showLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        resultPanelLayout.setVerticalGroup(
            resultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(resultPanelLayout.createSequentialGroup()
                .addComponent(showLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 22, Short.MAX_VALUE))
        );

        jLabel1.setText("Multiplier Effect:");

        javax.swing.GroupLayout inputPanelLayout = new javax.swing.GroupLayout(inputPanel);
        inputPanel.setLayout(inputPanelLayout);
        inputPanelLayout.setHorizontalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(PlayButton, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stabilityLabel)
                    .addComponent(populationLabel)
                    .addComponent(donationLabel)
                    .addComponent(crisisLabel)
                    .addComponent(severityLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputPanelLayout.createSequentialGroup()
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sizeField, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stabilitySlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(donationField, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(severitySlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(calamity, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(displayLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .addGroup(inputPanelLayout.createSequentialGroup()
                .addComponent(resultPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        inputPanelLayout.setVerticalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(donationField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(donationLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sizeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(populationLabel))
                .addGap(18, 18, 18)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addComponent(stabilitySlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(crisisLabel)
                            .addComponent(calamity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(stabilityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(severitySlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(severityLabel))
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(PlayButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(displayLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(resultPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(inputPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(inputPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>                        

    private void donationFieldKeyTyped(java.awt.event.KeyEvent evt) {                                       
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c)||(c==KeyEvent.VK_BACK_SPACE) || c == KeyEvent.VK_DELETE)) {
            evt.consume();
        }
    }                                      

    private void sizeFieldKeyTyped(java.awt.event.KeyEvent evt) {                                   
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c)||(c==KeyEvent.VK_BACK_SPACE) || c == KeyEvent.VK_DELETE)) {
            evt.consume();
        }
    }                                  

    private void PlayButtonActionPerformed(java.awt.event.ActionEvent evt) {                                           
        //donation, double pop, double houseSize, double stability, double severity, boolean conflict
                double donation = Double.parseDouble(donationField.getText());
                double population = Double.parseDouble(sizeField.getText());
                double stability = stabilitySlider.getValue();
                double severity = severitySlider.getValue();
                boolean conflict = (calamity.getSelectedIndex()==0);
                Game game=new Game(donation, population, stability,severity,conflict);
        game.makeVillage();
        double c=game.calcC();
                double me = 1/(1-c);
                me = Math.round(me*10)/10.0;
                //System.out.println(donation);
                //System.out.println(population);
                //System.out.println(houseSize);
                //System.out.println(stability);
                //System.out.println(severity);
                //System.out.println(conflict);
                //System.out.println(calamity.getSelectedIndex());
                displayLabel.setText(Double.toString(me));
                
               DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Saver"+Double.toString(game.getProc(0))+"%", game.getProc(0));
        dataset.setValue("Spender"+Double.toString(game.getProc(1))+"%", game.getProc(1));
        dataset.setValue("Importer"+Double.toString(game.getProc(2))+"%", game.getProc(2));
        dataset.setValue("Entrepreneur"+Double.toString(game.getProc(3))+"%", game.getProc(3));
        dataset.setValue("Trader"+Double.toString(game.getProc(4))+"%", game.getProc(4));
        dataset.setValue("Child"+Double.toString(game.getProc(5))+"%", game.getProc(5));
        
        JFreeChart chart = ChartFactory.createPieChart(
                "Behaviour profile proportions",  // chart title
                dataset,             // data
                true,               // include legend
                true,
                false
            );
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.02);

        
        java.awt.image.BufferedImage image = chart.createBufferedImage(300, 300);
        showLabel.setIcon(new ImageIcon(image));
    }                                          

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new gui().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton PlayButton;
    private javax.swing.JComboBox<String> calamity;
    private javax.swing.JLabel crisisLabel;
    private javax.swing.JLabel displayLabel;
    private javax.swing.JTextField donationField;
    private javax.swing.JLabel donationLabel;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel populationLabel;
    private javax.swing.JPanel resultPanel;
    private javax.swing.JLabel severityLabel;
    private javax.swing.JSlider severitySlider;
    private javax.swing.JLabel showLabel;
    private javax.swing.JTextField sizeField;
    private javax.swing.JLabel stabilityLabel;
    private javax.swing.JSlider stabilitySlider;
    // End of variables declaration                   
}
