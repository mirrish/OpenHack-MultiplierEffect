import java.util.Random;

public class Behaviour {
	String name;
	double c;
	double proc;
	double min;
	double max;
	double donationpp;
	
	Random rand=new Random();
	
	public Behaviour(String name, double donationpp) {
		this.name=name;
		this.donationpp=donationpp;
		
		switch(name){
			case("SAVER"): c=0.4;min=20;max=60;
			break;
			case("SPENDER"): c=0.9;min=20;max=60;
			if (donationpp>=100){
				max=max+5;
				min=min+5;
			}
			break;
			case("IMPORTER"): c=0.1;min=6;max=12;
			break;
			case("ENTREPRENOUR"): c=-1;min=5;max=15;
			if (donationpp>100){ // Om donationen är hög
				max=max+20;
				min=min+20;
			}
			break;
			case("TRADER"): c=-1;min=20;max=30;
			break;
			case("CHILD"): c=-1;min=35;max=45;
			break;
		}
		randomProc();
	}
	
	private void randomProc(){	
		this.proc=((rand.nextInt((int) (max-min))+min))/100;
	}
	
	public double getProc(){
	    return Math.round(proc*10.0)/10.0;
	}
	
	public void setProc(double proc){
		this.proc=proc;
	}
	
	
	public double getC(){
		return c;
	}
	
	public double getMin(){
		return min;
	}
	
	public double getMax(){
		return max;
	}
	
	public String getName(){
		return name;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
